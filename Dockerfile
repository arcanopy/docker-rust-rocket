FROM amazonlinux:2
RUN curl https://sh.rustup.rs > rustup.sh;
RUN sh rustup.sh --default-toolchain none -y;

ENV PATH=$PATH:~/.cargo/bin

RUN rustup install nightly-2018-06-20;
RUN rustup default nightly-2018-06-20;

RUN yum update -y
RUN yum groupinstall -y "Development Tools"
RUN yum install -y  https://download.postgresql.org/pub/repos/yum/10/redhat/rhel-7-x86_64/pgdg-redhat10-10-2.noarch.rpm
RUN sed -i "s/rhel-\$releasever-\$basearch/rhel-latest-x86_64/g" "/etc/yum.repos.d/pgdg-10-redhat.repo"
RUN yum install -y postgresql10
RUN yum clean all

CMD ["/bin/bash"]
